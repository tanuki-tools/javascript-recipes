# Javascript recipes to use GitLab API

> :construction: WIP / it's an old

## Environment

Many examples show you how to set environmental variables for things like the GitLab URL or TOKEN.  You can also copy the file `.env.example` to `.env` in your project root and then set those variables in there.

## Users scripts

See the scripts in the `./users` directory

- **count.instance.users**: counts the users of a GitLab instance
- **instance.activities**: allows to retrieve the users of a GitLab instance with information about max role on project and group
- **all.users**: search all users on several GitLab instances and check if each user exists several times and extract data in various files
- **intersection.users**: extract common users between 2 GitLab instances

### Requirements

- NodeJS (the used version for developement is `v11.2.0`)

## More to come

> stay tuned



## Disclaimer :wave:

These scripts are provided for educational purposes. This project is subject to an opensource license (feel free to fork it, and make it better). You can modify the scripts according to your needs. This project is not part of the GitLab support. However, if you need help do not hesitate to create an issue in the associated project, or better to propose a Merge Request.
:warning: Before using these scripts in production, check the consistency of the results on test instances, and of course make backups.
