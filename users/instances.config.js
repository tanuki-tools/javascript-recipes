const GLClient = require("../libs/gl-cli").GLClient
module.exports = {
  instances: [
      new GLClient({
        baseUri: `${process.env.URL1}/api/v4`,
        token: process.env.TOKEN1
      })
    , new GLClient({
        baseUri: `${process.env.URL2}/api/v4`,
        token: process.env.TOKEN2
      })   
  ]

}