/**
 * Search common users between 2 instances
 * Export the "intersection" to an xlsx file
 * and a Json file
 */

const GLClient = require("../libs/gl-cli").GLClient
const fs = require("fs")
const url = require('url')

const jsonexport = require('jsonexport');

let gitLabClient01 = new GLClient({
  baseUri: `${process.env.URL1}/api/v4`,
  token: process.env.TOKEN1
})

let gitLabClient02 = new GLClient({
  baseUri: `${process.env.URL2}/api/v4`,
  token: process.env.TOKEN2
})

async function getAllUsers({glClient, perPage, page}) {
  return glClient   // return a promise
    .get({path: `/users?per_page=${perPage}&page=${page}`})
    .then(response => response.data)
    .catch(error => error)
}


// await is only valid in async function
async function getInstanceUsers(glClient) {
  try {
    var stop = false, page = 1, users = []
    while (!stop) {
      await getAllUsers({glClient, perPage:20, page:page}).then(someUsers => {
        
        users = users.concat(someUsers.map(user => {
          // TODO: get the user role
          return { 
            id:user.id,
            username:user.username, 
            web_url:user.web_url, 
            state:user.state, 
            email:user.email,
            is_admin:user.is_admin,
            external:user.external,
            can_create_group: user.can_create_group,
            can_create_project: user.can_create_project

          }
        }))
        if(someUsers.length == 0) { stop = true }
      })
      page +=1
    }
    return users
  } catch (error) {
    console.log("😡:", error)
  }
}


async function batch(gitLabClient01, gitLabClient02) {
  console.time("GitLab")
  var arrGLInstance01
    , arrGLInstance02

  await getInstanceUsers(gitLabClient01).then(res => arrGLInstance01=res)
  await getInstanceUsers(gitLabClient02).then(res => arrGLInstance02=res)
  

  // ----- Search for common users between 2 instances -----
  let intersectionUsers = arrGLInstance01
    .filter(
      gl01user => arrGLInstance02
        .find(gl02user => gl02user.email == gl01user.email)
    )

  let usersReport = intersectionUsers.map(user => {
    let gl02User = arrGLInstance02.find(gl02user => gl02user.email == user.email)
    return {
      username: user.username,
      email: user.email,
      instances: [
        {
          id: user.id,
          web_url: user.web_url, 
          state: user.state, 
          is_admin:user.is_admin,
          external:user.external,
          can_create_group: user.can_create_group,
          can_create_project: user.can_create_project,
          last_activity_on: user.last_activity_on,
          last_activity_at: user.last_activity_at
        },
        {
          id: gl02User.id,
          web_url: gl02User.web_url, 
          state: gl02User.state, 
          is_admin:gl02User.is_admin,
          external:gl02User.external,
          can_create_group: gl02User.can_create_group,
          can_create_project: gl02User.can_create_project,
          last_activity_on: gl02User.last_activity_on,
          last_activity_at: gl02User.last_activity_at        
        }
      ]
    }
  })
  console.timeEnd("GitLab")
  return usersReport

}

batch(gitLabClient01, gitLabClient02).then(jsonResults => {
  console.log(jsonResults.length, "users")
  console.time("Export")

  let instanceName01 = url.parse(gitLabClient01.baseUri).hostname
  let instanceName02 = url.parse(gitLabClient02.baseUri).hostname

  //fs.writeFileSync(`./common_users/${instanceName01}-${instanceName02}-data.json`, JSON.stringify(jsonResults))
  fs.writeFileSync(`./common_users/data.json`, JSON.stringify(jsonResults))


  let jsonForExport = jsonResults.map(user => {
    return {
      username: user.username,
      email: user.email,
      id_on_instance01: user.instances[0].id,
      id_on_instance02: user.instances[1].id,       
      state_on_instance01: user.instances[0].state,
      state_on_instance02: user.instances[1].state, 
      is_admin_on_instance01: user.instances[0].is_admin,
      is_admin_on_instance02: user.instances[1].is_admin,            
      instance01_url: user.instances[0].web_url,
      instance02_url: user.instances[1].web_url,
      external_on_instance01: user.instances[0].external,
      external_on_instance02: user.instances[1].external,      
      can_create_group_on_instance01: user.instances[0].can_create_group,
      can_create_group_on_instance02: user.instances[1].can_create_group,     
      can_create_project_on_instance01: user.instances[0].can_create_project,
      can_create_project_on_instance02: user.instances[1].can_create_project, 
      last_activity_on_instance01: user.instances[0].last_activity_on,
      last_activity_at_instance02: user.instances[1].last_activity_at,  
    }
  })


  jsonexport(jsonForExport, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    //fs.writeFileSync(`./common_users/${instanceName01}-${instanceName02}-data.csv`, csv)
    fs.writeFileSync(`./common_users/data.csv`, csv)
  })


  console.timeEnd("Export")

})

