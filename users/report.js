/**
 * Search common users between 2 instances
 * Export the "intersection" to an xlsx file
 * and a Json file
 */
const DEBUG = process.env.DEBUG || false

const GLClient = require("../libs/gl-cli").GLClient
const fs = require("fs")
const url = require('url')

const jsonexport = require('jsonexport');

let instances = require("./all.users.config").instances

/*
async function getAllUsers({glClient, perPage, page}) {
  return glClient   // return a promise
    .get({path: `/users?per_page=${perPage}&page=${page}`})
    .then(response => response.data)
    .catch(error => error)
}
*/

async function getAllUsers({glClient, perPage, page}) {
  return glClient.getAllUsers({perPage, page})
}


async function getAllProjects({glClient, perPage, page}) {
  return glClient.getAllProjects({perPage, page})
}

async function getAllGroups({glClient, perPage, page}) {
  return glClient.getAllGroups({perPage, page})
}

async function getActivities({glClient}) {
  return glClient.getActivities()
}

// === Members ===
// https://docs.gitlab.com/ee/api/members.html
// Get Members of a project
async function getProjectMembers(glClient, {projectId}) {
  return glClient.getProjectMembers({projectId})
}

// Get Members of a project
async function getGroupMembers(glClient, {groupId}) {
  return glClient.getGroupMembers({groupId})
}

async function getLicenceInfo({glClient}) {
  //console.log("+++++++++++++++++++++++++++++++++++++++")
  //console.log(glClient)
  //console.log("+++++++++++++++++++++++++++++++++++++++")
  return glClient   // return a promise
    .get({path: `/license`})
    .then(response => response.data)
    .catch(error => error)
}


// await is only valid in async function
async function getInstanceUsers({glClient}) {
  if(DEBUG) {
    console.log("------------------------------------------------------------------------------")
    console.log("🐯 getInstanceUsers / glClient", glClient.baseUri)
  }

  try {
    var stop = false, page = 1, users = []
    while (!stop) {
      await getAllUsers({glClient, perPage:20, page:page}).then(someUsers => {
        /* === user information ====
        { id: 1,
          name: 'Administrator',
          username: 'root',
          state: 'active',
          avatar_url:
          'https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon',
          web_url: 'http://gitlab-3.test/root',
          created_at: '2018-12-10T14:58:14.458Z',
          bio: null,
          location: null,
          public_email: '',
          skype: '',
          linkedin: '',
          twitter: '',
          website_url: '',
          organization: null,
          last_sign_in_at: '2018-12-10T15:02:29.652Z',
          confirmed_at: '2018-12-10T14:58:14.168Z',
          last_activity_on: '2018-12-10',
          email: 'admin@example.com',
          theme_id: 1,
          color_scheme_id: 1,
          projects_limit: 100000,
          current_sign_in_at: '2018-12-10T15:02:29.652Z',
          identities: [],
          can_create_group: true,
          can_create_project: true,
          two_factor_enabled: false,
          external: false,
          private_profile: null,
          is_admin: true,
          shared_runners_minutes_limit: null 
        }
        */
        // --- TRACE ---
        if(DEBUG) {
          console.log(
            "🐱 getAllUsers / page:", page, 
            "someUsers", someUsers.map(item => { 
              return `${item.id} ${item.name} ${item.username} ${item.state}`
              //return { id: item.id, name: item.name, username: item.username, state: item.state }
          }))
        }

        users = users.concat(someUsers.map(user => {
          // TODO: get the user role
          return { 
            id: user.id,
            name: user.name,
            username: user.username, 
            web_url:user.web_url, 
            email: user.email,
            instance: url.parse(user.web_url).hostname, // url.parse(glClient.baseUri).hostname
            web_url: user.web_url, 
            state: user.state, 
            is_admin: user.is_admin,
            external: user.external,
            can_create_group: user.can_create_group,
            can_create_project: user.can_create_project,
            processed: false,
            created_at: user.created_at,
            last_sign_in_at: user.last_sign_in_at,
            confirmed_at: user.confirmed_at,
            current_sign_in_at: user.current_sign_in_at,
            last_activity_on: user.last_activity_on,
          }
        }))
        if(someUsers.length == 0) { stop = true }
      })
      page +=1
    }

    /* === user information ====
    [ { id: 1,
        username: 'root',
        email: 'admin@example.com',
        instance: 'gitlab-3.test',
        web_url: 'http://gitlab-3.test/root',
        state: 'active',
        is_admin: true,
        external: false,
        can_create_group: true,
        can_create_project: true,
        processed: false,
        created_at: '2018-12-10T14:58:14.458Z',
        last_sign_in_at: '2018-12-10T15:02:29.652Z',
        confirmed_at: '2018-12-10T14:58:14.168Z',
        current_sign_in_at: '2018-12-10T15:02:29.652Z' } ]
    */

    // --- TRACE ---
    if(DEBUG) {
      console.log("🐸 getInstanceUsers / users", users.map(item => { 
        return `${item.id} ${item.name} ${item.username} ${item.state} isAdmin: ${item.is_admin}`
        //return { id: item.id, name: item.name, username: item.username, state: item.state }
      }))
      console.log("------------------------------------------------------------------------------")
    }

    return users
  } catch (error) {
    console.log("😡:", error)
  }
}


/**
 * Get all groups  of the GitLab instance
 * @param {*} glClient 
 */
async function getInstanceGroups({glClient}) {
  try {
    var stop = false, page = 1, groups = []
    while (!stop) {
      await getAllGroups({glClient, perPage:20, page:page}).then(someGroup => {
        groups = groups.concat(someGroup.map(group => {
          return group
        }))
        if(someGroup.length == 0) { stop = true }
      })
      page +=1
    }
    return groups
  } catch (error) {
    console.log("😡:", error)
  }
}


/**
 * Get all projects of the GitLab instance
 * @param {*} glClient 
 */
async function getInstanceProjects({glClient}) {
  try {
    var stop = false, page = 1, projects = []
    while (!stop) {
      await getAllProjects({glClient, perPage:20, page:page}).then(someProject => {
        
        projects = projects.concat(someProject.map(project => {
          return project
        }))
        if(someProject.length == 0) { stop = true }
      })
      page +=1
    }
    return projects
  } catch (error) {
    console.log("😡:", error)
  }
}

// url.parse(gitLabClient01.baseUri).hostname
async function batch(instances) {
  let allUsers = []
  let all_activities = []


  for(var i in instances) {

    let instanceGitLabClient = instances[i]
    
    /*
    { starts_at: '2018-02-26',
      expires_at: '2020-02-26',
      licensee:
      { Name: 'Philippe Charrière',
        Email: 'pcharriere@gitlab.com',
        Company: 'Bots.Garden' },
      add_ons: {},
      user_limit: 20,
      active_users: 17 }
    */

    /**
     * USERS INFORMATION
     */

    await getLicenceInfo({glClient: instanceGitLabClient}).then(res => {
      if(DEBUG) {
        console.log("-------------- Licence Information -------------------------------------------")
        console.log("instance: ", url.parse(instanceGitLabClient.baseUri).hostname)
        console.log("starts: ", res.starts_at)
        console.log("expires: ", res.expires_at)
        console.log("user_limit: ", res.user_limit)
        console.log("active_users: ", res.active_users) // without guests?
        console.log("------------------------------------------------------------------------------")
      }
    })

    await getInstanceUsers({glClient: instanceGitLabClient}).then(res => {
      allUsers = allUsers.concat(res)
    })

    /**
     * ACTIVITIES INFORMATION
     */
    let instanceName = url.parse(instanceGitLabClient.baseUri).hostname
    console.time("GitLab")
    var arrGLInstance
      , activitiesGLInstance
      , projectsGLInstance
      , groupsGLInstance
      , projectsMembersGLInstance
      , groupsMembersGLInstance

    await getInstanceUsers({glClient: instanceGitLabClient}).then(res => arrGLInstance=res)
    await getActivities({glClient: instanceGitLabClient}).then(res => activitiesGLInstance=res)
    await getInstanceProjects({glClient: instanceGitLabClient}).then(res => projectsGLInstance=res)
    await getInstanceGroups({glClient: instanceGitLabClient}).then(res => groupsGLInstance=res)
          
    fs.writeFileSync(`./activities/projects_${instanceName}.json`, JSON.stringify(projectsGLInstance))
    fs.writeFileSync(`./activities/groups_${instanceName}.json`, JSON.stringify(groupsGLInstance))
    jsonexport(projectsGLInstance, {rowDelimiter: ';'}, function(err, csv){
      if(err) return console.log(err)
      fs.writeFileSync(`./activities/projects_${instanceName}.csv`, csv)
    })
    jsonexport(groupsGLInstance, {rowDelimiter: ';'}, function(err, csv){
      if(err) return console.log(err)
      fs.writeFileSync(`./activities/groups_${instanceName}.csv`, csv)
    })        

     /* access_level
        10 => Guest access
        20 => Reporter access
        30 => Developer access
        40 => Maintainer access
        50 => Owner access # Only valid for groups
    */
    let getAccessLevelLabel = (level) => {
      switch(level) {
        case 10:
          return "guest"
          break;
        case 20:
          return "reporter"
          break;
        case 30:
          return "developer"
          break;
        case 40:
          return "maintener"
          break;
        case 50:
          return "owner"
          break;
        default:
          return ""
      }
    } // end of getAccessLevelLabel


      // ----- find the max role on project for each user -----
      projectsMembersGLInstance = []
      for(var i in projectsGLInstance) {
        await getProjectMembers(instanceGitLabClient, {projectId: projectsGLInstance[i].id})
          .then(res => {
            projectsMembersGLInstance = projectsMembersGLInstance.concat(res.map(item => {
              item.access_level_label = getAccessLevelLabel(item.access_level)
              return item
            }))
          })
      }

      fs.writeFileSync(`./activities/projects_members_${instanceName}.json`, JSON.stringify(projectsMembersGLInstance))
      jsonexport(projectsMembersGLInstance, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./activities/projects_members_${instanceName}.csv`, csv)
      })

      // ----- find the max role on Group for each user -----
      groupsMembersGLInstance = []
      for(var i in groupsGLInstance) {
        await getGroupMembers(instanceGitLabClient, {groupId: groupsGLInstance[i].id})
          .then(res => {
            groupsMembersGLInstance = groupsMembersGLInstance.concat(res.map(item => {
              item.access_level_label = getAccessLevelLabel(item.access_level)
              return item
            }))
          })
      }

      fs.writeFileSync(`./activities/groups_members_${instanceName}.json`, JSON.stringify(groupsMembersGLInstance))
      jsonexport(groupsMembersGLInstance, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./activities/groups_members_${instanceName}.csv`, csv)
      })

      /** activities affectations */
      arrGLInstance.forEach(userInstance => {

        userInstance.project_access_level = null
        userInstance.project_access_level_label = null
        userInstance.group_access_level = null
        userInstance.group_access_level_label = null

        var userInProject = projectsMembersGLInstance.find(user => user.id == userInstance.id)

        if(userInProject) {
          var userInAllProjects = projectsMembersGLInstance.filter(user => user.id == userInstance.id)
          let maxLevel = Math.max.apply(Math, userInAllProjects.map(o => { return o.access_level }))
          //userInstance.project_access_level = userInProject.access_level
          userInstance.project_access_level = maxLevel
          userInstance.project_access_level_label = getAccessLevelLabel(maxLevel)
          
        }
        
        var userInGroup = groupsMembersGLInstance.find(user => user.id == userInstance.id)
        if(userInGroup) {
          var userInAllGroups = groupsMembersGLInstance.filter(user => user.id == userInstance.id)
          let maxLevel = Math.max.apply(Math, userInAllGroups.map(o => { return o.access_level }))
          //userInstance.group_access_level = userInGroup.access_level
          userInstance.group_access_level = maxLevel
          userInstance.group_access_level_label = getAccessLevelLabel(maxLevel)
        }
      })      
      

      fs.writeFileSync(`./activities/data_${instanceName}.json`, JSON.stringify(arrGLInstance))
      jsonexport(arrGLInstance, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./activities/data_${instanceName}.csv`, csv)
      })    

      all_activities = all_activities.concat(arrGLInstance)

      console.timeEnd("GitLab")

  } // end for

  if(DEBUG) { console.log("👋  Total users for all instances:", allUsers.length) }

  fs.writeFileSync(`./activities/data_all_instances.json`, JSON.stringify(all_activities))
  jsonexport(all_activities, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./activities/data_all_instances.csv`, csv)
  })

  return { allUsers, all_activities }
}

batch(instances).then(results => {
  if(DEBUG) { console.log("👋  (Control) Total users for all instances:", results.allUsers.length) }
  console.time("Export")

  let usersInOnlyOneInstance = []
  let usersInSeveralInstances = []
  let remaining = []

  results.allUsers.filter(user=>user.state!=="blocked").forEach(user => {

    //let search = allUsers.filter(item => ((item.email == user.email) && (!user.processed)))  // test username too?
    let search = results.allUsers.filter(item => item.email == user.email && !user.processed)

    /*
      The user exists only one time (in only one instance)
    */
    if(search.length==1) {
      // put the user in the "only one instance" list 
      usersInOnlyOneInstance.push({ 
          id: user.id,
          username: user.username, 
          email: user.email,
          instance: user.instance,
          state: user.state
      })
      user.processed = true
    } 

    /*
      The user exists several times (in two or more instances)
    */
    if(search.length>1) {
      search.forEach(element => {
        //allUsers.find(item => element.email == item.email && element.instance == item.instance).processed = true
        element.processed = true
      });
       // put the user in the "several instances" list 
      let record = { 
        id: user.id,
        username: user.username, 
        email: user.email,
        instance: search.map(item => item.instance).join("|"),
        state: search.map(item => item.state).join("|")
      }
      // the user exist several time
      usersInSeveralInstances.push(record)
    }


    /*
      The I cannot find the user because it has been marked as processed
      I save it in a list: "remaining" list then I can control at the end
      example:
      if bob exists in G1, G2 and G3
        => bob will be present one time in the "several instances" list
        => bob will be present two times in "remaining" list

        => then the total number of users is 3 = nb users of "several instances" + nb users of "remaining"
    */    
    if(search.length==0) {
      // the user has been already processed
      remaining.push({ 
          id: user.id,
          username: user.username, 
          email: user.email,
          instance: user.instance,
          state: user.state
      })      
      user.processed = true
    }  
    
  });

  
  fs.writeFileSync(`./all_users/remaining.json`, JSON.stringify(remaining))

  jsonexport(remaining, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./all_users/remaining.csv`, csv)
  })
  
  fs.writeFileSync(`./all_users/users-in-only-one-instance.json`, JSON.stringify(usersInOnlyOneInstance))

  jsonexport(usersInOnlyOneInstance, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./all_users/users-in-only-one-instance.csv`, csv)
  })

  fs.writeFileSync(`./all_users/users-in-several-instances.json`, JSON.stringify(usersInSeveralInstances))

  jsonexport(usersInSeveralInstances, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./all_users/users-in-several-instances.csv`, csv)
  })

  fs.writeFileSync(`./all_users/all-users.json`, JSON.stringify(results.allUsers))

  jsonexport(results.allUsers, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./all_users/all-users.csv`, csv)
  })

  let control = remaining.length+usersInSeveralInstances.length+usersInOnlyOneInstance.length
  let blocked = results.allUsers.length - control 

  //all_activities
  let guests_users = results.all_activities.filter(user => {
    return (user.project_access_level==10 && user.group_access_level==10) ||
    (user.project_access_level==10 && user.group_access_level==null) ||
    (user.project_access_level==null && user.group_access_level==10)
  })

  if(DEBUG) { console.log("😃 Guest users", guests_users) }

  console.log()
  console.log("==================== REPORT ==================================================")
  console.log(" > Total users in all instances             :", results.allUsers.length, "active inactive and blocked")
  console.log("------------------------------------------------------------------------------")
  console.log(" > Total users in only one instance         :", usersInOnlyOneInstance.length, "users")
  console.log(" > Total users in several instances         :", usersInSeveralInstances.length, "(main admins included)")
  console.log(" > Total remaining users (- blocked)        :", remaining.length)
  console.log("------------------------------------------------------------------------------")
  console.log(" > Control (active + inactive)              :", control)
  console.log(" > Blocked users                            :", blocked)
  console.log("------------------------------------------------------------------------------")
  console.log(" >   Main administrators                    :", instances.length, "(one per instance)")
  console.log(" > + Users in several instances (- 1 admin) :", usersInSeveralInstances.length - 1)
  console.log(" > + Users in one instances                 :", usersInOnlyOneInstance.length)
  //console.log(" > - Blocked users                          :", blocked)
  console.log(" > ______________________________________   : _________")
  console.log(" >   TOTAL                                  :", usersInSeveralInstances.length + (instances.length - 1) + usersInOnlyOneInstance.length)
  console.log(" >   Guest users                            :", guests_users.length)
  console.log(" >   TOTAL (Ultimate users - guests)        :", usersInSeveralInstances.length + (instances.length - 1) + usersInOnlyOneInstance.length - guests_users.length) 
  console.log("==============================================================================")
  console.timeEnd("Export")

  

})

