# Issues Scrips

## project.issues

The `project.issues` script searches for and returns issues from a given GitLab project

### run it

```
TOKEN="your-gitlab-token" URL="your-gitlab-url" node projects.issues project-namespace [options]
```

or use [../README.md#environment](a .env file to define your token and URL)

> For options details, run

``` 
node projecats.issues --help
```

### Options :chef:

-  `--open`                        Show only open issues
-  `--closed`                      Show only closed issues
-  `--assigned`                    Only return issues assigned to me
-  `--created`                     Only show issues created by me
-  `-s` or `--search` [search]        Search for issues by terms
-  `-l` or `--labels` [label1,label2] Search by issue label(s)
-  `-m` or `--milestone` [milestone]  Search for issues with a particular milestone
-  `-e` or `--emoji` [emoji name]     Search for issues you marked with a certain emoji
-  `-o` or `--out` [json/csv]         Output the list to JSON or CSV
-  `-h` or `--help`                   output usage information

## Disclaimer :wave:

These scripts are provided for educational purposes. This project is subject to an opensource license (feel free to fork it, and make it better). You can modify the scripts according to your needs. This project is not part of the GitLab support. However, if you need help do not hesitate to create an issue in the associated project, or better to propose a Merge Request.
:warning: Before using these scripts in production, check the consistency of the results on test instances, and of course make backups.


## BTW

:wave: all the tokens are fake tokens 