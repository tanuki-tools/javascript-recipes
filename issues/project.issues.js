#!/usr/bin/env node
const GLClient = require("../libs/gl-cli").GLClient
var program = require('commander');
const fs = require("fs")
const jsonexport = require('jsonexport');

async function fetchIssues(gitLabConnection, {perPage, page, project, opts}) {
  return gitLabConnection.fetchProjectIssues({perPage, page, project, opts})
}

async function getProjectIssues(gitLabConnection, project, opts) {
  console.log(">> get issues from the project")
  try {
    var stop = false, page = 1, issues = []

    while (!stop) {
      await fetchIssues(gitLabConnection, {perPage:100, page:page, project: project, opts: opts}).then(someIssues => {
        if(someIssues.length == 0) { stop = true }
        issues = issues.concat(someIssues)
      })
      page +=1
    }
    console.log(">> issues ready")
    return issues
  } catch (error) {
    console.log("😡:", error)
  }
}

async function batch(gitLabConnection, project, opts) {
  return await getProjectIssues(gitLabConnection, project, opts)
}

let gitLab = new GLClient({
  baseUri: `${process.env.URL}/api/v4`,
  token: process.env.TOKEN
})

let project = process.argv[2]

program
  .version('0.0.1')
  .usage('<project ID or namespace> [options]')
  .option('--open', 'Show only open issues')
  .option('--closed', 'Show only closed issues')
  .option('-s, --search [search terms]', 'Search for issues by terms')
  .option('-l, --labels [label1,label2]', 'Search by issue label(s)')
  .option('-m, --milestone [milestone]', 'Search for issues with a particular milestone')
  .option('--assigned', 'Only return issues assigned to me')
  .option('--created', 'Only show issues created by me')
  .option('-e, --emoji [emoji name]', 'Search for issues you marked with a certain emoji')
  .option('-o, --out [json/csv]', 'Output the list to JSON or CSV')
  .parse(process.argv);

if (program.open) program.state = 'opened'
if (program.closed) program.state = 'closed'

if (program.assigned) program.scope = 'assigned_to_me'
if (program.created) program.scope = 'created_by_me'

opts = {
    state: program.state,
    search: program.search, 
    labels: program.labels,
    milestone: program.milestone,
    emoji: program.emoji,
    scope: program.scope
}

if (project) {
    batch(gitLab, project, opts).then(results => {
        let openIssues = results.filter(issue => issue.state == "opened")
        let closedIusses = results.filter(issue => issue.state == "closed")
        
        console.log(`===== ${project} =====`)
        console.log(`open issues:   ${openIssues.length}`)
        console.log(`closed issues: ${closedIusses.length}`)
        console.log('')

        if (program.out) {
            if (program.out == 'csv') {
                let csvforexport = results.map(issue => {
                    return { 
                      id: issue.id,
                      iid: issue.id,
                      project_id: issue.project_id,
                      title: issue.title,
                      state: issue.state,
                      created_at: issue.created_at,
                      updated_at: issue.updated_at,
                      closed_at: issue.closed_at,
                      closed_by: issue.closed_by,
                      labels: issue.labels,
                      milestone: issue.milestone,
                    }
                  })
                jsonexport(csvforexport, 
                        {rowDelimiter: ',', textDelimiter: "'", forceTextDelimiter: true}, 
                        function(err, csv){
                            if(err) return console.log(err)
                            fs.writeFileSync(`./out/data.csv`, csv)
                    })
            } else {
                fs.writeFileSync(`./out/data.json`, JSON.stringify(results))
            }
        }
        
      }).catch(error => {
        console.log("😡:", error)
      })
} else {
    console.log("🙀 Project not specified.  See --help for more")
    process.exit(1)
}


